
/*
 * rl78_g23_boot_loader.c
 *
 * Created on: 7 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "linker_config.h"

#include "debug/asserts.h"
#include "ll_crc/ll_crc.h"
#include "ll_time/ll_time.h"
#include "ll_nvm/ll_nvm.h"

#include "l3/l3_packet.h"
#include "l3/l3_network.h"

#include "events/events.h"
#include "events/event_queue.h"
#include "dispatcher/dispatcher.h"
#include "updaters/boot_loader_updater.h"
#include "updaters/application_updater.h"

#include "r_smc_entry.h"

////////////////////////////
// PRIVATE CONSTANTS
//
#define WAIT_TIME_IN_MS 5000



////////////////////////////
// PRIVATE TYPES
//
enum
{
	STATE_TYPE_IDLING,
	STATE_TYPE_PROCESSING_PROTOCOL,
	STATE_TYPE_UPDATING_BOOT_LOADER,
	STATE_TYPE_UPDATING_APPLICATION,
	STATE_TYPES_COUNT
};

typedef uint8_t StateType_t;

typedef struct
{
	void(*enter)(void); // Can be NULL
	void(*run)(EventHandle_t event); // Must not be NULL!
	void(*exit)(void); // Can be NULL
} State_t;

typedef State_t* StateHandle_t;



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//
static LL_Time_t time0_;
static bool stayInBootLoader_;



////////////////////////////
// PRIVATE METHODS
//
signed int Main_bootLoaderInvoke(void);

static void transitionState_(const StateType_t stateType);
static void processEvents_(void);

static void Idle_enter(void);
static void Idle_run(EventHandle_t event);
static void Idle_exit(void);



////////////////////////////
// STATES
//
static State_t states_[STATE_TYPES_COUNT] =
{
	[STATE_TYPE_IDLING] =
	{
		.enter = Idle_enter,
		.run = Idle_run,
		.exit = Idle_exit
	},
	[STATE_TYPE_PROCESSING_PROTOCOL] =
	{
		.enter = Dispatcher_enter,
		.run = Dispatcher_run,
		.exit = Dispatcher_exit
	},
	[STATE_TYPE_UPDATING_BOOT_LOADER] =
	{
		.enter = BootLoaderUpdater_enter,
		.run = BootLoaderUpdater_run,
		.exit = BootLoaderUpdater_exit
	},
	[STATE_TYPE_UPDATING_APPLICATION] =
	{
		.enter = ApplicationUpdater_enter,
		.run = ApplicationUpdater_run,
		.exit = ApplicationUpdater_exit
	}
};

static StateType_t currentState_;



////////////////////////////
// IMPLEMENTATION
//
signed int Main_bootLoaderInvoke(void)
{
	EI();

	/* {
		LL_Nvm_init(LL_NVM_FREQUENCY_32MHz);
	} */

	LL_Crc_init();
	LL_Time_init();
	Dispatcher_init();
	transitionState_(STATE_TYPE_IDLING);

	stayInBootLoader_ = false;
	time0_ = LL_Time_getMs();

	// TODO: remove:
	stayInBootLoader_ = true;

	while (stayInBootLoader_ || LL_Time_getElapsedMs(time0_) < WAIT_TIME_IN_MS)
	{
		Dispatcher_listen();
		processEvents_();
	}

	return 0;
}

static void Idle_enter(
	void)
{
}

static void Idle_run(
	EventHandle_t event)
{
	switch (event->type)
	{
		case EVENT_TYPE_RECEIVED_PACKET:
		{
			transitionState_(STATE_TYPE_PROCESSING_PROTOCOL);
			(void)EventQueue_enqueue(*event);
		} break;

		case EVENT_TYPE_UPDATE_BOOT_LOADER:
		{
			transitionState_(STATE_TYPE_UPDATING_BOOT_LOADER);
		} break;

		case EVENT_TYPE_UPDATE_APPLICATION:
		{
			transitionState_(STATE_TYPE_UPDATING_APPLICATION);
		} break;

		default:
		{
			transitionState_(STATE_TYPE_IDLING);
		} break;
	}
}

static void Idle_exit(
	void)
{
}

static void transitionState_(
	const StateType_t stateType)
{
	// The exit routine of current state before transitioning.
	StateHandle_t oldState = &states_[currentState_];
	if (oldState->exit) oldState->exit();

	currentState_ = stateType;

	// The enter routine of a "transition-to" state.
	StateHandle_t newState = &states_[currentState_];
	if (newState->enter) newState->enter();
}

static void processEvents_(
	void)
{
	Event_t event = EventQueue_dequeue();

	if (event.type != EVENT_TYPE_NONE)
	{
		stayInBootLoader_ = true; // Stay in the boot loader flag
		StateHandle_t state = &states_[currentState_];
		DEBUG_ASSERT((int8_t)state->run);
		state->run(&event);
	}
}
