
/*
 * l3_network.h
 *
 * Created on: 18 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __L3__L3_NETWORK_H__
#define __L3__L3_NETWORK_H__

#include "l2/l2_network.h"

#include "l3/l3_packet.h"

#include <stdbool.h>
#include <stdint.h>

enum
{
	L3_TX_STATUS_OK,
	L3_TX_STATUS_BUSY,
	L3_TX_STATUS_FAILED,
	L3_TX_STATUSES_COUNT
};

typedef uint8_t L3_TxStatus_t;

void L3_Network_create(
	L2_NetworkHandle_t leftL2Network,
	L2_NetworkHandle_t rightL2Network);

bool L3_Network_run(void);

bool L3_Network_receivePacket(L3_PacketHandle_t l3Packet);

L3_TxStatus_t L3_Network_sendPacket(L3_PacketHandle_t l3Packet);

#endif /* __L3__L3_NETWORK_H__ */
