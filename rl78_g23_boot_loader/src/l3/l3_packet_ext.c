
/*
 * l3_packet_ext.c
 *
 * Created on: 18 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "l3/l3_packet_ext.h"

#include "debug/asserts.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void L3_EnumerateCellsPacket_handle(L3_PacketHandle_t l3Packet)
{
	DEBUG_ASSERT(l3Packet);

	// NOTE: Enumerate Cells packet can only be send as a broadcast packet
	//       as it assumes that cell modules are not enumerated (addressed)
	//       yet.
	if (l3Packet->header.transferType != L3_TRANSFER_TYPE_BROADCAST)
	{
		return;
	}

	// NOTE: Do not enumerate cell modules on the way back!
	if (l3Packet->header.status == L3_PACKET_STATUS_FLIPPED)
	{
		return;
	}

	L3_EnumerateCellsPacketHandle_t innerPacket =
		(L3_EnumerateCellsPacketHandle_t)L3_Packet_referenceBody(l3Packet);

	if (innerPacket->apply)
	{
		extern /* static */ void setCellId_(const L3_CellId_t cellId);
		setCellId_(innerPacket->cellsCount);
	}

	innerPacket->cellsCount++;
}

void L3_GetSerialNumberPacket_handle(L3_PacketHandle_t l3Packet)
{
	DEBUG_ASSERT(l3Packet);

	// NOTE: Get Serial Number packet can only be send as a unicast packet
	//       as it only holds one serial number value.
	if (l3Packet->header.transferType != L3_TRANSFER_TYPE_UNICAST)
	{
		return;
	}

	// TODO: copy these over the liker config?
	#define configRL78G23_UNIQUE_ID_START 0xEFFC0
	#define configRL78G23_UNIQUE_ID_LENGTH 16

	L3_GetSerialNumberPacketHandle_t innerPacket =
		(L3_GetSerialNumberPacketHandle_t)L3_Packet_referenceBody(l3Packet);
	memcpy((void*)(innerPacket->number),
		(const void*)(configRL78G23_UNIQUE_ID_START + configRL78G23_UNIQUE_ID_LENGTH - 3), 3
	);
}

// TODO: Implement packet handling functions!
