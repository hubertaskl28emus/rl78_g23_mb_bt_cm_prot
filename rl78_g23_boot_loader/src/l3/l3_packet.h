
/*
 * l3_packet.h
 *
 * Created on: 18 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __L3__L3_PACKET_H__
#define __L3__L3_PACKET_H__

#include "l2/l2_network.h"

#include <stdint.h>

enum
{
	L3_PACKET_DIRECTION_FROM_BOTTOM_TO_TOP = 0,
	L3_PACKET_DIRECTION_FROM_TOP_TO_BOTTOM,
	L3_PACKET_DIRECTIONS_COUNT
};

typedef uint8_t L3_PacketDirection_t;
typedef L3_PacketDirection_t* L3_PacketDirectionHandle_t;
#define L3_PACKET_DIRECTION_FIELD_BITS_COUNT 1
_Static_assert(L3_PACKET_DIRECTIONS_COUNT <= (1 << L3_PACKET_DIRECTION_FIELD_BITS_COUNT),
	"L3_PacketDirection_t enum values cannot exceed 1 as they can only be packed into 1 bit field!");

enum
{
	L3_PACKET_STATUS_UNTOUCHED = 0,
	L3_PACKET_STATUS_FLIPPED,
	L3_PACKET_STATUSES_COUNT
};

typedef uint8_t L3_PacketStatus_t;
typedef L3_PacketStatus_t* L3PacketStatusHandle_t;
#define L3_PACKET_STATUS_FIELD_BITS_COUNT 1
_Static_assert(L3_PACKET_STATUSES_COUNT <= (1 << L3_PACKET_STATUS_FIELD_BITS_COUNT),
	"L3_PacketStatus_t enum values cannot exceed 1 as they can only be packed into 1 bit field!");

enum
{
	L3_PACKET_TYPE_ENUMERATE_CELLS = 0,
	L3_PACKET_TYPE_GET_SERIAL_NUMBER,
	L3_PACKET_TYPE_GET_SW_VERSION, // TODO: implement L3_PACKET_TYPE_GET_SW_VERSION!
	L3_PACKET_TYPE_GET_HW_STATS, // TODO: implement L3_PACKET_TYPE_GET_HW_STATS!
	L3_PACKET_TYPE_CHANGE_RUNNING_PARTITION, // TODO: implement L3_PACKET_TYPE_CHANGE_RUNNING_PARTITION!
	L3_PACKET_TYPE_RAW, // TODO: implement L3_PACKET_TYPE_RAW!
	L3_PACKET_TYPES_COUNT
};

typedef uint8_t L3_PacketType_t;
#define L3_PACKET_TYPE_FIELD_BITS_COUNT 4
_Static_assert(L3_PACKET_TYPES_COUNT <= (1 << L3_PACKET_TYPE_FIELD_BITS_COUNT),
	"L3_PacketType_t enum values cannot exceed 15 as they can be packed into 4 bits field!");

typedef L2_PacketLength_t L3_PacketLength_t;
#define L3_PACKET_BODY_LENGTH_FIELD_BITS_COUNT 8
_Static_assert(sizeof(L3_PacketLength_t) <= (1 << L3_PACKET_BODY_LENGTH_FIELD_BITS_COUNT),
	"L3_PacketLength_t value cannot exceed 255 as it can only be packed into 8 bits field!");

typedef uint8_t L3_CellId_t;
#define L3_PACKET_CELL_ID_FIELD_BITS_COUNT 8
_Static_assert(sizeof(L3_CellId_t) <= (1 << L3_PACKET_CELL_ID_FIELD_BITS_COUNT),
	"L3_CellId_t value cannot exceed 255 as it can only be packed into 8 bits field!");
#define L3_CELL_ID_UNDEFINED 0xFF

enum
{
	L3_TRANSFER_TYPE_BROADCAST,
	L3_TRANSFER_TYPE_UNICAST,
	L3_TRANSFER_TYPE_MULTICAST,
	L3_TRANSFER_TYPES_COUNT
};

typedef uint8_t L3_TransferType_t;
#define L3_PACKET_TRANSFER_TYPE_FIELD_BITS_COUNT 2
_Static_assert(L3_TRANSFER_TYPES_COUNT <= (1 << L3_PACKET_TRANSFER_TYPE_FIELD_BITS_COUNT),
	"L3_TransferType_t value cannot exceed 3 as it can only be packed into 2 bits field!");

typedef struct
{
	L3_TransferType_t transferType 	: L3_PACKET_TRANSFER_TYPE_FIELD_BITS_COUNT;
	L3_PacketType_t packetType 		: L3_PACKET_TYPE_FIELD_BITS_COUNT;
	L3_PacketDirection_t direction 	: L3_PACKET_DIRECTION_FIELD_BITS_COUNT;
	L3_PacketStatus_t status 		: L3_PACKET_STATUS_FIELD_BITS_COUNT;
	L3_PacketLength_t bodyLength 	: L3_PACKET_BODY_LENGTH_FIELD_BITS_COUNT;
} L3_PacketHeader_t;

typedef L3_PacketHeader_t* L3_PacketHeaderHandle_t;
_Static_assert(sizeof(L3_PacketHeader_t) == 2, "L3_PacketHeader_t is of incorrect size in bytes!");



// Broadcast packet definitions
#define L3_BROADCAST_PACKET_BODY_CAPACITY MAX_PACKET_DATA_LENGTH - sizeof(L3_PacketHeader_t)

typedef struct
{
	uint8_t data[L3_BROADCAST_PACKET_BODY_CAPACITY];
} L3_BroadcastPacketBody_t;

typedef struct
{
	L3_BroadcastPacketBody_t body;
} L3_BroadcastPacket_t;



// Unicast packet definitions
typedef struct
{
	L3_CellId_t recipient 			: L3_PACKET_CELL_ID_FIELD_BITS_COUNT;
} L3_UnicastPacketHeader_t;

typedef L3_UnicastPacketHeader_t* L3_UnicastPacketHeaderHandle_t;
_Static_assert(sizeof(L3_UnicastPacketHeader_t) == 1, "L3_UnicastPacketHeader_t is of incorrect size in bytes!");
#define L3_UNICAST_PACKET_BODY_CAPACITY \
	MAX_PACKET_DATA_LENGTH - sizeof(L3_PacketHeader_t) - sizeof(L3_UnicastPacketHeader_t)

typedef struct
{
	uint8_t data[L3_UNICAST_PACKET_BODY_CAPACITY];
} L3_UnicastPacketBody_t;

typedef struct
{
	L3_UnicastPacketHeader_t header;
	L3_UnicastPacketBody_t body;
} L3_UnicastPacket_t;



// Multicast packet definitions
typedef struct
{
	L3_CellId_t bottomRecipient 	: L3_PACKET_CELL_ID_FIELD_BITS_COUNT;
	L3_CellId_t topRecipient 		: L3_PACKET_CELL_ID_FIELD_BITS_COUNT;
} L3_MulticastPacketHeader_t;

typedef L3_MulticastPacketHeader_t* L3_MulticastPacketHeaderHandle_t;
_Static_assert(sizeof(L3_MulticastPacketHeader_t) == 2, "L3_MulticastPacketHeader_t is of incorrect size in bytes!");
#define L3_MULTICAST_PACKET_BODY_CAPACITY \
	MAX_PACKET_DATA_LENGTH - sizeof(L3_PacketHeader_t) - sizeof(L3_MulticastPacketHeader_t)

typedef struct
{
	uint8_t data[L3_MULTICAST_PACKET_BODY_CAPACITY];
} L3_MulticastPacketBody_t;

typedef struct
{
	L3_MulticastPacketHeader_t header;
	L3_MulticastPacketBody_t body;
} L3_MulticastPacket_t;


// L3 Packet definitions
typedef struct
{
	L3_PacketHeader_t header;

	union
	{
		L3_BroadcastPacket_t broadcastPacket;
		L3_UnicastPacket_t unicastPacket;
		L3_MulticastPacket_t multicastPacket;
	};
} L3_Packet_t;

typedef L3_Packet_t* L3_PacketHandle_t;

uint8_t* L3_Packet_referenceBody(const L3_PacketHandle_t l3Packet);

L3_PacketLength_t L3_Packet_getPacketLength(const L3_PacketHandle_t l3Packet);

bool L3_Packet_isRecipientOfThePacket(
	const L3_CellId_t recipient,
	const L3_PacketHandle_t l3Packet);

bool L3_Packet_isFirstTripLastRecipient(
	const L3_CellId_t recipient,
	const L3_PacketHandle_t l3Packet);

#endif /* __L3__L3_PACKET_H__ */
