
/*
 * l3_packet.c
 *
 * Created on: 18 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "l3/l3_packet.h"

#include "debug/asserts.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
uint8_t* L3_Packet_referenceBody(const L3_PacketHandle_t l3Packet)
{
	DEBUG_ASSERT(l3Packet);
	uint8_t* result;

	switch (l3Packet->header.transferType)
	{
		case L3_TRANSFER_TYPE_BROADCAST:
		{
			result = l3Packet->broadcastPacket.body.data;
		} break;

		case L3_TRANSFER_TYPE_UNICAST:
		{
			result = l3Packet->unicastPacket.body.data;
		} break;

		case L3_TRANSFER_TYPE_MULTICAST:
		{
			result = l3Packet->multicastPacket.body.data;
		} break;

		// Should never ever reach this block
		default:
		{
			DEBUG_ASSERT(0);
			result = 0; // To prevent compiler errors/warnings
		} break;
	}

	return result;
}

L3_PacketLength_t L3_Packet_getPacketLength(const L3_PacketHandle_t l3Packet)
{
	DEBUG_ASSERT(l3Packet);
	L3_PacketLength_t result;

	switch (l3Packet->header.transferType)
	{
		case L3_TRANSFER_TYPE_BROADCAST:
		{
			result = sizeof(L3_PacketHeader_t) + l3Packet->header.bodyLength;
		} break;

		case L3_TRANSFER_TYPE_UNICAST:
		{
			result = sizeof(L3_PacketHeader_t) + sizeof(L3_UnicastPacketHeader_t) + l3Packet->header.bodyLength;
		} break;

		case L3_TRANSFER_TYPE_MULTICAST:
		{
			result = sizeof(L3_PacketHeader_t) + sizeof(L3_MulticastPacketHeader_t) + l3Packet->header.bodyLength;
		} break;

		// Should never ever reach this block
		default:
		{
			DEBUG_ASSERT(0);
			result = 0; // To prevent compiler errors/warnings
		} break;
	}

	return result;
}

bool L3_Packet_isRecipientOfThePacket(
	const L3_CellId_t recipient,
	const L3_PacketHandle_t l3Packet)
{
	DEBUG_ASSERT(l3Packet);
	bool result = false;

	switch (l3Packet->header.transferType)
	{
		case L3_TRANSFER_TYPE_BROADCAST:
		{
			// When broadcasting a packet, everyone is a recipient of the packet:
			result = true;
		} break;

		case L3_TRANSFER_TYPE_UNICAST:
		{
			result = (recipient == l3Packet->unicastPacket.header.recipient);
		} break;

		case L3_TRANSFER_TYPE_MULTICAST:
		{
			result = (
				recipient >= l3Packet->multicastPacket.header.bottomRecipient && 
				recipient <= l3Packet->multicastPacket.header.topRecipient
			);
		} break;

		// Should never ever reach this block
		default:
		{
			DEBUG_ASSERT(0);
			result = false; // To prevent compiler errors/warnings
		} break;
	}

	return result;
}

bool L3_Packet_isFirstTripLastRecipient(
	const L3_CellId_t recipient,
	const L3_PacketHandle_t l3Packet)
{
	DEBUG_ASSERT(l3Packet);

	if (l3Packet->header.status == L3_PACKET_STATUS_FLIPPED)
	{
		return false;
	}

	bool result = false;
	const L3_PacketDirection_t direction = l3Packet->header.direction;

	switch (l3Packet->header.transferType)
	{
		case L3_TRANSFER_TYPE_BROADCAST:
		{
			// When broadcasting a packet, there is no thing as first/second trip and end recipients:
			result = false;
		} break;

		case L3_TRANSFER_TYPE_UNICAST:
		{
			result = (recipient == l3Packet->unicastPacket.header.recipient);
		} break;

		case L3_TRANSFER_TYPE_MULTICAST:
		{
			result = (
				((recipient == l3Packet->multicastPacket.header.bottomRecipient) && (direction == L3_PACKET_DIRECTION_FROM_TOP_TO_BOTTOM)) || 
				((recipient == l3Packet->multicastPacket.header.topRecipient) && (direction == L3_PACKET_DIRECTION_FROM_BOTTOM_TO_TOP))
			);
		} break;

		// Should never ever reach this block
		default:
		{
			DEBUG_ASSERT(0);
			result = false; // To prevent compiler errors/warnings
		} break;
	}

	return result;
}
