
/*
 * l3_packet_ext.h
 *
 * Created on: 18 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __L3__L3_PACKET_EXT_H__
#define __L3__L3_PACKET_EXT_H__

#include "l3/l3_packet.h"

#include <stdbool.h>
#include <stdint.h>

// Commons
typedef void(*L3_PacketHandlerFunction_t)(L3_PacketHandle_t);



// Enumerate Cells Packet
typedef struct
{
	L3_CellId_t cellsCount;
	uint8_t apply : 8;
} L3_EnumerateCellsPacket_t;

typedef L3_EnumerateCellsPacket_t* L3_EnumerateCellsPacketHandle_t;

void L3_EnumerateCellsPacket_handle(L3_PacketHandle_t l3Packet);



// Enumerate Cells Packet
typedef struct
{
	uint32_t number[3];
} L3_GetSerialNumberPacket_t;

typedef L3_GetSerialNumberPacket_t* L3_GetSerialNumberPacketHandle_t;

void L3_GetSerialNumberPacket_handle(L3_PacketHandle_t l3Packet);


// TODO: Define packet types and functions for handling them!

#endif /* __L3__L3_PACKET_EXT_H__ */
