
/*
 * ll_nvm.h
 *
 * Created on: 25 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_NVM__LL_NVM_H__
#define __LL_NVM__LL_NVM_H__

#include <stdbool.h>
#include <stdint.h>

enum
{
	LL_NVM_FREQUENCY_1MHz = 0x00u,
	LL_NVM_FREQUENCY_2MHz = 0x01u,
	LL_NVM_FREQUENCY_3MHz = 0x02u,
	LL_NVM_FREQUENCY_4MHz = 0x03u,
	LL_NVM_FREQUENCY_5MHz = 0x04u,
	LL_NVM_FREQUENCY_6MHz = 0x05u,
	LL_NVM_FREQUENCY_7MHz = 0x06u,
	LL_NVM_FREQUENCY_8MHz = 0x07u,
	LL_NVM_FREQUENCY_9MHz = 0x08u,
	LL_NVM_FREQUENCY_10MHz = 0x09u,
	LL_NVM_FREQUENCY_11MHz = 0x0Au,
	LL_NVM_FREQUENCY_12MHz = 0x0Bu,
	LL_NVM_FREQUENCY_13MHz = 0x0Cu,
	LL_NVM_FREQUENCY_14MHz = 0x0Du,
	LL_NVM_FREQUENCY_15MHz = 0x0Eu,
	LL_NVM_FREQUENCY_16MHz = 0x0Fu,
	LL_NVM_FREQUENCY_17MHz = 0x10u,
	LL_NVM_FREQUENCY_18MHz = 0x11u,
	LL_NVM_FREQUENCY_19MHz = 0x12u,
	LL_NVM_FREQUENCY_20MHz = 0x13u,
	LL_NVM_FREQUENCY_21MHz = 0x14u,
	LL_NVM_FREQUENCY_22MHz = 0x15u,
	LL_NVM_FREQUENCY_23MHz = 0x16u,
	LL_NVM_FREQUENCY_24MHz = 0x17u,
	LL_NVM_FREQUENCY_25MHz = 0x18u,
	LL_NVM_FREQUENCY_26MHz = 0x19u,
	LL_NVM_FREQUENCY_27MHz = 0x1Au,
	LL_NVM_FREQUENCY_28MHz = 0x1Bu,
	LL_NVM_FREQUENCY_29MHz = 0x1Cu,
	LL_NVM_FREQUENCY_30MHz = 0x1Du,
	LL_NVM_FREQUENCY_31MHz = 0x1Eu,
	LL_NVM_FREQUENCY_32MHz = 0x1Fu,
	LL_NVM_FREQUENCIES_COUNT
};

typedef uint8_t LL_NvmFrequency_t;

enum
{
	LL_NVM_DF_ACCESS_MODE_DISABLE = 0x00u,
	LL_NVM_DF_ACCESS_MODE_ENABLE = 0x01u,
	LL_NVM_DF_ACCESS_MODES_COUNT
};

typedef uint8_t LL_NvmDfAccessMode_t;

typedef enum
{
	LL_NVM_FLASH_MODE_UNPROGRAMMABLE   = 0x00u,
	LL_NVM_FLASH_MODE_CODE_PROGRAMMING = 0x01u,
	LL_NVM_FLASH_MODE_DATA_PROGRAMMING = 0x02u,
	LL_NVM_FLASH_MODES_COUNT
} LL_NvmFlashMode_t;

void LL_Nvm_init(const LL_NvmFrequency_t frequencyMHz);
void LL_Nvm_setDataFlashAccessMode(const LL_NvmDfAccessMode_t accessMode);
bool LL_Nvm_setFlashMode(const LL_NvmFlashMode_t flashMode);
bool LL_Nvm_checkFlashMode(const LL_NvmFlashMode_t flashMode);

void LL_Nvm_Df_readBytes(
	const uint16_t address,
	uint8_t* const buffer,
	const uint8_t count);
void LL_Nvm_Df_eraseBlock(const uint16_t blockIndex);
void LL_Nvm_Df_blankCheck(const uint16_t blockIndex);
void LL_Nvm_Df_writeBytes(
	const uint16_t address,
	const uint8_t bytes[1]);

void LL_Nvm_Cf_readBytes(
	const uint16_t address,
	uint8_t* const buffer,
	const uint8_t count);
void LL_Nvm_Cf_eraseBlock(const uint16_t blockIndex);
void LL_Nvm_Cf_blankCheck(const uint16_t blockIndex);
void LL_Nvm_Cf_writeBytes(
	const uint16_t address,
	const uint8_t bytes[4]);

bool LL_Nvm_await(void);

#endif /* __LL_NVM__LL_NVM_H__ */
