
/*
 * event_queue.h
 *
 * Created on: 12 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __EVENTS__EVENT_QUEUE_H__
#define __EVENTS__EVENT_QUEUE_H__

#include "events/events.h"

#include <stdbool.h>
#include <stdint.h>

bool EventQueue_enqueue(const Event_t event);

Event_t EventQueue_dequeue(void);

#endif /* __EVENTS__EVENT_QUEUE_H__ */
