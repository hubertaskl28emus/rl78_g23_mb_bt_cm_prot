
/*
 * event_queue.c
 *
 * Created on: 12 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "events/event_queue.h"

#include "mcu/rl78_g23/register_access/llvm/iodefine_ext.h"
#include "mcu/rl78_g23/register_access/llvm/iodefine.h"

////////////////////////////
// PRIVATE CONSTANTS
//
#define EVENT_QUEUE_SIZE 16
#define MOD_QUEUE(i) ((i) % EVENT_QUEUE_SIZE)



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//
static Event_t eventQueue_[EVENT_QUEUE_SIZE];
static uint8_t eventQueueHead_ = 0;
static uint8_t eventQueueTail_ = 0;



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
bool EventQueue_enqueue(const Event_t event)
{
	bool result = false;

	DI();
	const uint8_t next = MOD_QUEUE(eventQueueHead_ + 1);

	if (next != eventQueueTail_)
	{
		eventQueue_[eventQueueHead_] = event;
		eventQueueHead_ = next;
		result = true;
	}

	EI();

	return result;
}

Event_t EventQueue_dequeue(void)
{
	Event_t result;
	result.type = EVENT_TYPE_NONE;

	DI();

	if (eventQueueHead_ != eventQueueTail_)
	{
		result = eventQueue_[eventQueueTail_];
		eventQueueTail_ = MOD_QUEUE(eventQueueTail_ + 1);
	}

	EI();

	return result;
}
