
/*
 * asserts.h
 *
 * Created on: 11 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __DEBUG__ASSERTS_H__
#define __DEBUG__ASSERTS_H__

#ifdef NDEBUG
#	define DEBUG_ASSERT(_expression)
#else
#	include <stdbool.h>
void Debug_assert(const bool expression);
#	define DEBUG_ASSERT(_expression) Debug_assert(_expression)
#endif /* NDEBUG */

#endif /* __DEBUG__ASSERTS_H__ */
