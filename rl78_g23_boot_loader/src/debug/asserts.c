
/*
 * asserts.c
 *
 * Created on: 11 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "debug/asserts.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
#ifdef NDEBUG
#else
void Debug_assert(const bool expression)
{
	if (!expression)
	{
		extern void PowerON_Reset(void);
		PowerON_Reset();
	}
}
#endif
