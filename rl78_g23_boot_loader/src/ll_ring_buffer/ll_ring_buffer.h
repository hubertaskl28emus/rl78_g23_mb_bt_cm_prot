
/*
 * ll_ring_buffer.h
 *
 * Created on: 8 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_RING_BUFFER__LL_RING_BUFFER_H__
#define __LL_RING_BUFFER__LL_RING_BUFFER_H__

#include <stdbool.h>
#include <stdint.h>

#define LL_RING_BUFFER_32B_SIZE 64

typedef struct
{
	uint8_t data[LL_RING_BUFFER_32B_SIZE];
	uint8_t head;
	uint8_t tail;
} LL_RingBuffer_t;

bool LL_RingBuffer_give(
	LL_RingBuffer_t* const ringBuffer,
	const uint8_t byte);

bool LL_RingBuffer_take(
	LL_RingBuffer_t* const ringBuffer,
	uint8_t* const byte);

#endif /* __LL_RING_BUFFER__LL_RING_BUFFER_H__ */
