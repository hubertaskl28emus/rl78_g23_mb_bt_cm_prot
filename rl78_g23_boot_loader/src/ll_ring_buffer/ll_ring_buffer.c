
/*
 * ll_ring_buffer.c
 *
 * Created on: 8 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "ll_ring_buffer/ll_ring_buffer.h"

#include "debug/asserts.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
bool LL_RingBuffer_give(
	LL_RingBuffer_t* const ringBuffer,
	const uint8_t byte)
{
	DEBUG_ASSERT(ringBuffer);
	uint8_t next = ringBuffer->head + 1;
	if (next >= LL_RING_BUFFER_32B_SIZE) next = 0;

	if (next == ringBuffer->tail)
	{
		return false;
	}

	ringBuffer->data[ringBuffer->head] = byte;
	ringBuffer->head = next;
	return true;
}

bool LL_RingBuffer_take(
	LL_RingBuffer_t* const ringBuffer,
	uint8_t* const byte)
{
	DEBUG_ASSERT(ringBuffer);
	DEBUG_ASSERT(byte);

	if (ringBuffer->head == ringBuffer->tail)
	{
		return false;
	}

	uint8_t next = ringBuffer->tail + 1;
	if(next >= LL_RING_BUFFER_32B_SIZE) next = 0;

	*byte = ringBuffer->data[ringBuffer->tail];
	ringBuffer->tail = next;
	return true;
}
