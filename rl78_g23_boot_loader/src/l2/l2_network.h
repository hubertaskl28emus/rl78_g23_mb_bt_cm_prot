
/*
 * l2_network.h
 *
 * Created on: 11 Sep 2023
 *     Author: Paulius Tumelis (paulius.tumelis@emusbms.com)
 */

#ifndef __L2__L2_NETWORK_H__
#define __L2__L2_NETWORK_H__

#include "ll_serial/ll_serial.h"

#include <stdbool.h>
#include <stdint.h>

#define SYNC_BYTE										0x55
#define BITS_IN_BYTE									8
#define MAX_BYTE_NUMBER									255
#define SEND_RETRY_COUNT								2

// Timeouts configuration
#define ENABLE_COMMUNICATION_FLOW_CONTROL				true	// ensures all bytes got successfuly received

#define	MAX_RETRANSMISSION_RETRIES						3		// How many retransmissions to await succeed
#define	TRANSMISSION_TIMEOUT							5000	// Delay in ms of SEND / RECEIVE ACK until considered as TIMEOUT
#define QUEUE_AWAIT_TIME								1000	// Delay in ms to wait until packet appears in queue, setting 10 ms, to avoid spam checks

// Structure L2_Packet_t configuration
#define	SEQUENCE_NUMBER_BITS_COUNT						3		// Delay in ms of SEND / RECEIVE ACK until considered as TIMEOUT
#define DATA_ID_BITS_COUNT								5		// Delay in ms to wait until packet appears in queue, setting 10 ms, to avoid spam checks
#define MAX_PACKET_DATA_LENGTH							64		// L2 Packet payload max allowed length

// Preprocessor phase checks and corrections
#if SEQUENCE_NUMBER_BITS_COUNT + DATA_ID_BITS_COUNT != BITS_IN_BYTE
#	error Wrong defined Sequence number and DataId packing
#endif

#if MAX_PACKET_DATA_LENGTH >= MAX_BYTE_NUMBER
#	error Exceeded packet max possible message size
#endif

enum L2_PacketType_t
{	
	///////////////////////////
	// Command types (0-8)
	///////////////////////////
	L2TYPE_PING = 	0,			// Layer 2 containing data type for pinging devices
	L2TYPE_ACK = 	1,			// Layer 2 containing data type for returning acknowledged status
	L2TYPE_NACK = 	2,			// Layer 2 containing data type for returning not acknowledged status

	////////////////////////////////
	// G1 CAN cells protocol (9-25)
	////////////////////////////////
	L2TYPE_RAW = 	9,

	L2TYPE_COUNT
};

typedef uint8_t L2_PacketLength_t;   // l2 Data packet maximum length , use it when passing as parameter! (not uint8_t)
typedef uint8_t L2_PacketType_t;     // l2 Data packet type variable, later on enum of types gets casted to it


//////////////////////////
//	PUBLIC ATTRIBUTES
//

typedef struct
{
	uint8_t syncByte;								// (1 byte) sync byte
	L2_PacketLength_t dataLength;							// (1 byte) data size identifier
	uint8_t seqNumber : SEQUENCE_NUMBER_BITS_COUNT;	// (SEQUENCE_NUMBER_BITS_COUNT) Sequence number
	L2_PacketType_t dataId : DATA_ID_BITS_COUNT;	// (DATA_ID_BITS_COUNT) data type identifier
} L2_PacketDescriptorHeader_t;

// Actual user defined l2 packet structure
typedef struct
{
	L2_PacketLength_t dataLength;							// (1 byte) data size identifier
	uint8_t seqNumber : SEQUENCE_NUMBER_BITS_COUNT;	// (SEQUENCE_NUMBER_BITS_COUNT) Sequence number
	L2_PacketType_t dataId : DATA_ID_BITS_COUNT;	// (DATA_ID_BITS_COUNT) data type identifier
	uint8_t data[MAX_PACKET_DATA_LENGTH];			// (64 bytes) data portion, which is identified by packetId
} L2_Packet_t;

typedef L2_Packet_t* L2_PacketHandle_t;

typedef enum
{
	SEND_OK,
	SEND_ERR_BUSY,
	SEND_ERR_FAIL,
} L2_SendingResult_t;

typedef enum
{
	RECV_OK,
	RECV_ONGOING,
	RECV_ERR_BUSY,
} L2_ReceptionResult_t;

typedef struct L2_Network_t L2_Network_t;
typedef L2_Network_t* L2_NetworkHandle_t;
typedef void(*L2_SendCompleteCb_f)(L2_PacketHandle_t packet, bool success);

typedef bool(*L2_Run_f)(L2_NetworkHandle_t handle);
typedef L2_SendingResult_t(*L2_PacketSend_f)(L2_NetworkHandle_t handle, const uint8_t* payload, const uint8_t payload_len, L2_SendCompleteCb_f cb);
typedef L2_ReceptionResult_t(*L2_PacketReceive_f)(L2_NetworkHandle_t handle, L2_PacketHandle_t * packet);
typedef void(*L2_PacketAck_f)(L2_NetworkHandle_t handle, L2_PacketHandle_t packet);
typedef void(*L2_PacketNack_f)(L2_NetworkHandle_t handle, L2_PacketHandle_t packet);

typedef struct
{
	L2_PacketSend_f send;
	L2_PacketAck_f sendAck;
	L2_PacketNack_f sendNak;
	L2_PacketReceive_f receive;
} L2_CommsInterface_t;

struct L2_Network_t
{
	// Callback functions
	void(*rxCallbackFunction)(L2_PacketHandle_t packetHandle, void* params);
	L2_SendCompleteCb_f sendCompleteCallback;

	// Callback data
	void* callbackParams;

	// abstract interface
	L2_PacketSend_f send;
	L2_PacketReceive_f receive;
	L2_Run_f run;
	
	// Descriptors
	L2_PacketDescriptorHeader_t acknowledgementHeader;   // Acknowledgement Packet header which tells about response itself, saving data bytes for RAM
};

// virtual function definitions
static inline L2_SendingResult_t L2_Network_sendPacket(
	L2_NetworkHandle_t handle,
	const uint8_t* payload,
	const uint8_t payload_len,
	L2_SendCompleteCb_f cb)
{
	return handle->send(handle, payload, payload_len, cb);
}

static inline L2_ReceptionResult_t L2_Network_receivePacket(
	L2_NetworkHandle_t handle,
	L2_PacketHandle_t* packet)
{
	return handle->receive(handle, packet);
}

static inline bool L2_Network_run(L2_NetworkHandle_t handle)
{
	return handle->run(handle);
}

#endif // __L2__L2_NETWORK_H__
