
/*
 * linker_config.h
 *
 * Created on: 4 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LINKER_CONFIG_H__
#define __LINKER_CONFIG_H__


/* ------------- The RL78 G23, R7F100GxG device memory map configuration ------------- */
#define configRL78G23_CFM_START			0x00000000	/* The code flash memory start     */
#define configRL78G23_CFM_LENGTH		0x00020000	/* The code flash memory length    */
#define configRL78G23_DFM_START			0x000F1000	/* The data flash memory start     */
#define configRL78G23_DFM_LENGTH		0x00002000	/* The data flash memory length    */
#define configRL78G23_MAM_START			0x000F3000	/* The mirror flash memory start   */
#define configRL78G23_MAM_LENGTH		0x00008F00	/* The mirror flash memory length  */
#define configRL78G23_RAM_START			0x000FBF00	/* The random access memory start  */
#define configRL78G23_RAM_LENGTH		0x00004000	/* The random access memory length */
#define configRL78G23_RS1_START			0x00020000	/* The first reserved area start   */
#define configRL78G23_RS1_LENGTH		0x000D0000	/* The first reserved area length  */
#define configRL78G23_RS2_START			0x000F0800	/* The second reserved area start  */
#define configRL78G23_RS2_LENGTH		0x00002000	/* The second reserved area length */
/* ----------------------------------------------------------------------------------- */


/* --------- The master boot/bootloader/application projects' configurations --------- */
#define configMASTER_BOOT_CFM_START		0x00000000	/* The code flash memory start     */
#define configMASTER_BOOT_CFM_LENGTH	0x00002000	/* The code flash memory length    */
#define configMASTER_BOOT_DFM_START		0x000F1000	/* The data flash memory start     */
#define configMASTER_BOOT_DFM_LENGTH	0x00000100	/* The data flash memory length    */
#define configMASTER_BOOT_MAM_START		0x000F3000	/* The mirror flash memory start   */
#define configMASTER_BOOT_MAM_LENGTH	0x00008F00	/* The mirror flash memory length  */
#define configMASTER_BOOT_RAM_START		0x000FBF00	/* The random access memory start  */
#define configMASTER_BOOT_RAM_LENGTH	0x00004000	/* The random access memory length */

#define configBOOT_LOADER_CFM_START		0x00002000	/* The code flash memory start     */
#define configBOOT_LOADER_CFM_LENGTH	0x00008000	/* The code flash memory length    */
#define configBOOT_LOADER_DFM_START		0x000F1100	/* The data flash memory start     */
#define configBOOT_LOADER_DFM_LENGTH	0x00000400	/* The data flash memory length    */
#define configBOOT_LOADER_MAM_START		0x000F3000	/* The mirror flash memory start   */
#define configBOOT_LOADER_MAM_LENGTH	0x00008F00	/* The mirror flash memory length  */
#define configBOOT_LOADER_RAM_START		0x000FBF00	/* The random access memory start  */
#define configBOOT_LOADER_RAM_LENGTH	0x00004000	/* The random access memory length */

#define configAPPLICATION_CFM_START		0x0000A000	/* The code flash memory start     */
#define configAPPLICATION_CFM_LENGTH	0x00016000	/* The code flash memory length    */
#define configAPPLICATION_DFM_START		0x000F1500	/* The data flash memory start     */
#define configAPPLICATION_DFM_LENGTH	0x00001B00	/* The data flash memory length    */
#define configAPPLICATION_MAM_START		0x000F3000	/* The mirror flash memory start   */
#define configAPPLICATION_MAM_LENGTH	0x00008F00	/* The mirror flash memory length  */
#define configAPPLICATION_RAM_START		0x000FBF00	/* The random access memory start  */
#define configAPPLICATION_RAM_LENGTH	0x00004000	/* The random access memory length */

#define configBOOT_LOADER_PRIORITY		0x00000001	/* The boot loader priority value  */
#define configAPPLICATION_PRIORITY		0x00000002	/* The application priority value  */
/* ----------------------------------------------------------------------------------- */


/* ----------------------------- Asserting Safety Checks ----------------------------- */
#if (configBOOT_LOADER_PRIORITY <= 0) || (configBOOT_LOADER_PRIORITY >= 255)
#	error "[fatal error]: boot loader's priority must be in the range of [1; 254]!"
#endif

#if (configAPPLICATION_PRIORITY <= 0) || (configAPPLICATION_PRIORITY >= 255)
#	error "[fatal error]: application's priority must be in the range of [1; 254]!"
#endif

#if configBOOT_LOADER_PRIORITY == configAPPLICATION_PRIORITY
#	error "[fatal error]: boot loader's and application's priorities must not match!"
#endif

#if (configMASTER_BOOT_TARGET_CFM_START < configRL78G23_CFM_START) || \
	((configMASTER_BOOT_TARGET_CFM_START + configMASTER_BOOT_TARGET_CFM_LENGTH - 1) >= configBOOT_LOADER_CFM_START)
#	error "[fatal error]: master boot's CFM is in an invalid region!"
#endif

#if (configBOOT_LOADER_TARGET_CFM_START < (configMASTER_BOOT_TARGET_CFM_START + configMASTER_BOOT_TARGET_CFM_LENGTH)) || \
	((configBOOT_LOADER_TARGET_CFM_START + configBOOT_LOADER_TARGET_CFM_LENGTH - 1) >= configAPPLICATION_CFM_START)
#	error "[fatal error]: bootloader's CFM is in an invalid region!"
#endif

#if (configAPPLICATION_TARGET_CFM_START < (configBOOT_LOADER_TARGET_CFM_START + configBOOT_LOADER_TARGET_CFM_LENGTH)) || \
	((configAPPLICATION_TARGET_CFM_START + configAPPLICATION_TARGET_CFM_LENGTH - 1) >= (configRL78G23_CFM_LENGTH - configRL78G23_CFM_START))
#	error "[fatal error]: application's CFM is in an invalid region!"
#endif

#if (configMASTER_BOOT_TARGET_DFM_START < (configRL78G23_DFM_LENGTH - configRL78G23_DFM_START)) || \
	((configMASTER_BOOT_TARGET_DFM_START + configMASTER_BOOT_TARGET_DFM_LENGTH - 1) >= configBOOT_LOADER_DFM_START)
#	error "[fatal error]: master boot's DFM is in an invalid region!"
#endif

#if (configBOOT_LOADER_TARGET_DFM_START < (configMASTER_BOOT_TARGET_DFM_START + configMASTER_BOOT_TARGET_DFM_LENGTH)) || \
	((configBOOT_LOADER_TARGET_DFM_START + configBOOT_LOADER_TARGET_DFM_LENGTH - 1) >= configAPPLICATION_DFM_START)
#	error "[fatal error]: bootloader's DFM is in an invalid region!"
#endif

#if (configAPPLICATION_TARGET_DFM_START < (configBOOT_LOADER_TARGET_DFM_START + configBOOT_LOADER_TARGET_DFM_LENGTH)) || \
	((configAPPLICATION_TARGET_DFM_START + configAPPLICATION_TARGET_DFM_LENGTH - 1) >= configRL78G23_DFM_LENGTH)
#	error "[fatal error]: application's DFM is in an invalid region!"
#endif
/* ----------------------------------------------------------------------------------- */


#endif /* __LINKER_CONFIG_H__ */
