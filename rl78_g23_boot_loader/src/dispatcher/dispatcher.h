
/*
 * dispatcher.h
 *
 * Created on: 22 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __DISPATCHER__DISPATCHER_H__
#define __DISPATCHER__DISPATCHER_H__

#include "events/events.h"

void Dispatcher_init(void);

void Dispatcher_listen(void);

void Dispatcher_enter(void);

void Dispatcher_run(EventHandle_t event);

void Dispatcher_exit(void);

#endif /* __DISPATCHER__DISPATCHER_H__ */
