
/*
 * dispatcher.c
 *
 * Created on: 22 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "dispatcher/dispatcher.h"

#include "ll_serial/ll_serial.h"

#include "l2/l2_serial.h"
#include "l2/l2_network.h"

#include "l3/l3_packet.h"
#include "l3/l3_network.h"

#include "events/event_queue.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//
static L2_SerialNetwork_t bottomL2SerialNetwork_;
static L2_SerialNetwork_t topL2SerialNetwork_;



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void Dispatcher_init(void)
{
	LL_Serial_open(LL_SERIAL_TYPE_UART1);
	LL_Serial_open(LL_SERIAL_TYPE_UART2);

	L2_SerialNetwork_create(&bottomL2SerialNetwork_, LL_SERIAL_TYPE_UART1);
	L2_SerialNetwork_create(&topL2SerialNetwork_, LL_SERIAL_TYPE_UART2);

	L3_Network_create(
		(L2_NetworkHandle_t)&bottomL2SerialNetwork_,
		(L2_NetworkHandle_t)&topL2SerialNetwork_
	);
}

void Dispatcher_listen(void)
{
	if (L3_Network_run())
	{
		L3_Packet_t l3Packet;

		if (L3_Network_receivePacket(&l3Packet))
		{
			Event_t event;
			event.type = EVENT_TYPE_RECEIVED_PACKET;
			event.l3Packet = l3Packet;
			(void)EventQueue_enqueue(event);
		}
	}
}

void Dispatcher_enter(void)
{
}

void Dispatcher_run(EventHandle_t event)
{
	if (event->type != EVENT_TYPE_RECEIVED_PACKET)
	{
		return;
	}

	L3_TxStatus_t status = L3_Network_sendPacket(&event->l3Packet);

	if (status != L3_TX_STATUS_OK)
	{
		const LL_Time_t time0 = LL_Time_getMs();
		while (LL_Time_getElapsedMs(time0) < 100 /* 400 */)
		{
			__asm("nop");
		}
	}
}

void Dispatcher_exit(void)
{
}
