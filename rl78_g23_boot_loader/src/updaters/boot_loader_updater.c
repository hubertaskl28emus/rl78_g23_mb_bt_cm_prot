
/*
 * boot_loader_updater.c
 *
 * Created on: 15 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "updaters/boot_loader_updater.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void BootLoaderUpdater_enter(void)
{
}

void BootLoaderUpdater_run(EventHandle_t event)
{
	(void)event;
}

void BootLoaderUpdater_exit(void)
{
}
