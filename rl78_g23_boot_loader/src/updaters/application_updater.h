
/*
 * application_updater.h
 *
 * Created on: 15 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __UPDATERS__APPLICATION_UPDATER_H__
#define __UPDATERS__APPLICATION_UPDATER_H__

#include "events/events.h"

void ApplicationUpdater_enter(void);

void ApplicationUpdater_run(EventHandle_t event);

void ApplicationUpdater_exit(void);

#endif /* __UPDATERS__APPLICATION_UPDATER_H__ */
