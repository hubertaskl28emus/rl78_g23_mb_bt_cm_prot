
/*
 * application_updater.c
 *
 * Created on: 15 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "updaters/application_updater.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void ApplicationUpdater_enter(void)
{
}

void ApplicationUpdater_run(EventHandle_t event)
{
	(void)event;
}

void ApplicationUpdater_exit(void)
{
}
