
/*
 * ll_crc.h
 *
 * Created on: 11 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_CRC__LL_CRC_H__
#define __LL_CRC__LL_CRC_H__

#include <stdint.h>

void LL_Crc_init(void);

uint16_t LL_Crc_calc16(
	const uint8_t* const data,
	const uint16_t length,
	const uint16_t initial);

#endif /* __LL_CRC__LL_CRC_H__ */
