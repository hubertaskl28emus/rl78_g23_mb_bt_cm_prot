
/*
 * ll_crc.c
 *
 * Created on: 11 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "ll_crc/ll_crc.h"

#include "mcu/rl78_g23/register_access/llvm/iodefine_ext.h"
#include "mcu/rl78_g23/register_access/llvm/iodefine.h"

////////////////////////////
// PRIVATE CONSTANTS
//
#define CRC_16_RESETIN  0x00		// 8bit register in reset
#define CRC_16_RESETOUT 0x0000		// 16bit register out reset



////////////////////////////
// PRIVATE TYPES
//



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void LL_Crc_init(
	void)
{
	CRCIN = CRC_16_RESETIN;
	CRCD = CRC_16_RESETOUT;
}

uint16_t LL_Crc_calc16(const uint8_t* const data, const uint16_t length,
					   const uint16_t initial)
{
	if (0 == length)
	{
		return initial;
	}
	
	DI();
	CRCD = initial;

	for (uint16_t index = 0; index < length; ++index)
	{
		CRCIN = data[index];
	}

	uint16_t value;
	value = CRCD;

	CRCD = 0;
	EI();

	return value;
}
