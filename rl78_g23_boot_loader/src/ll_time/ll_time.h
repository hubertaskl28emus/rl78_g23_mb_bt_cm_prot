
/*
 * ll_time.h
 *
 * Created on: 8 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_TIME__LL_TIME_H__
#define __LL_TIME__LL_TIME_H__

#include <stdint.h>

typedef uint32_t LL_Time_t;

void LL_Time_init(void);

LL_Time_t LL_Time_getMs(void);

LL_Time_t LL_Time_getElapsedMs(const LL_Time_t start);

void LL_Time_delay(const LL_Time_t milliseconds);

#endif /* __LL_TIME__LL_TIME_H__ */
