
/*
 * ll_serial.c
 *
 * Created on: 8 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "ll_serial/ll_serial.h"

#include "debug/asserts.h"
#include "ll_ring_buffer/ll_ring_buffer.h"

#include "smc_gen/general/r_cg_macrodriver.h"
#include "smc_gen/Config_UART1/Config_UART1.h"
#include "smc_gen/Config_UART2/Config_UART2.h"

////////////////////////////
// PRIVATE CONSTANTS
//



////////////////////////////
// PRIVATE TYPES
//
typedef struct
{
	void(*create)(void);
	void(*start)(void);
	void(*stop)(void);
	MD_STATUS(*send)(uint8_t* const, uint16_t);
} LL_SerialApi_t;

typedef struct
{
	LL_SerialApi_t api;
	LL_RingBuffer_t rxBuffer;
	LL_SerialTxCallback_t txCallback;
	bool txComplete;
} LL_SerialDescriptor_t;



////////////////////////////
// PRIVATE TYPE CONSTANTS
//



////////////////////////////
// PRIVATE ATTRIBUTES
//
static LL_SerialDescriptor_t serialDescriptors_[LL_SERIAL_TYPES_COUNT] =
{
	[LL_SERIAL_TYPE_UART1] =
	{
		.api.create = R_Config_UART1_Create,
		.api.start  = R_Config_UART1_Start,
		.api.stop   = R_Config_UART1_Stop,
		.api.send   = R_Config_UART1_Send
	},
	[LL_SERIAL_TYPE_UART2] =
	{
		.api.create = R_Config_UART2_Create,
		.api.start  = R_Config_UART2_Start,
		.api.stop   = R_Config_UART2_Stop,
		.api.send   = R_Config_UART2_Send
	},
};



////////////////////////////
// PRIVATE METHODS
//



////////////////////////////
// IMPLEMENTATION
//
void LL_Serial_open(const LL_SerialType_t serialType)
{
	LL_SerialDescriptor_t* const serialDescriptor = &serialDescriptors_[serialType];

	serialDescriptor->rxBuffer.head = 0;
	serialDescriptor->rxBuffer.tail = 0;

	serialDescriptor->txComplete = true;

	serialDescriptor->api.create();
	serialDescriptor->api.start();
}

bool LL_Serial_sendBlocking(
	const LL_SerialType_t serialType,
	const uint8_t* const txBuffer,
	const uint16_t txLength,
	const LL_Time_t timeout)
{
	DEBUG_ASSERT(txBuffer);
	DEBUG_ASSERT(txLength > 0);

	LL_SerialDescriptor_t* const serialDescriptor = &serialDescriptors_[serialType];
	serialDescriptor->txCallback = LL_SERIAL_TX_CALLBACK(NULL, NULL);
	const LL_Time_t time0 = LL_Time_getMs();

	for (uint16_t index = 0; index < txLength; ++index)
	{
		do
		{
			if (serialDescriptor->txComplete)
			{
				serialDescriptor->txComplete = false;
				const MD_STATUS status =
					serialDescriptor->api.send((uint8_t* const)(txBuffer + index), 1);

				if (status != MD_OK)
				{
					serialDescriptor->txComplete = true;
					return false;
				}
				else
				{
					break;
				}
			}
			else
			{
				continue;
			}
		} while (LL_Time_getElapsedMs(time0) < timeout);
	}

	return true;
}

bool LL_Serial_sendNonBlocking(
	const LL_SerialType_t serialType,
	const uint8_t* const txBuffer,
	const uint16_t txLength,
	const LL_SerialTxCallback_t txCallback)
{
	DEBUG_ASSERT(txBuffer);
	DEBUG_ASSERT(txLength > 0);

	LL_SerialDescriptor_t* const serialDescriptor = &serialDescriptors_[serialType];
	serialDescriptor->txCallback = txCallback;

	const MD_STATUS status =
		serialDescriptor->api.send((uint8_t* const)txBuffer, txLength);
	return MD_OK == status;
}

bool LL_Serial_receiveByte(
	const LL_SerialType_t serialType,
	uint8_t* const byte,
	const LL_Time_t timeout)
{
	DEBUG_ASSERT(byte);
	LL_SerialDescriptor_t* const serialDescriptor = &serialDescriptors_[serialType];
	const LL_Time_t time0 = LL_Time_getMs();

	do
	{
		if (LL_RingBuffer_take(&serialDescriptor->rxBuffer, byte))
		{
			return true;
		}
	} while (LL_Time_getElapsedMs(time0) < timeout);

	return false;
}

void LL_Serial_onReceived_(
	const LL_SerialType_t serialType,
	const uint8_t byte)
{
	LL_SerialDescriptor_t* const serialDescriptor = &serialDescriptors_[serialType];
	(void)LL_RingBuffer_give(&serialDescriptor->rxBuffer, byte);
}

void LL_Serial_onSent_(const LL_SerialType_t serialType)
{
	LL_SerialDescriptor_t* const serialDescriptor = &serialDescriptors_[serialType];
	serialDescriptor->txComplete = true;

	// NOTE: should never ever enter this when "blocking send call" is performed!
	if (serialDescriptor->txCallback.function)
	{
		serialDescriptor->txCallback.function(
			serialDescriptor->txCallback.arguments
		);
	}
}

void LL_Serial_onError_(
	const LL_SerialType_t serialType,
	const LL_SerialError_t errorType)
{
	if (errorType & LL_SERIAL_ERROR_STOP_BIT)
	{
		switch (serialType)
		{
			case LL_SERIAL_TYPE_UART1:
			{
				P7_bit.no5 = 0;
				NOP(); NOP(); NOP();
				P7_bit.no5 = 1;
			} break;

			case LL_SERIAL_TYPE_UART2:
			{
				P3_bit.no0 = 0;
				NOP(); NOP(); NOP();
				P3_bit.no0 = 1;
			} break;

			default:
			{
				NOP();
			} break;
		}
	}
}
