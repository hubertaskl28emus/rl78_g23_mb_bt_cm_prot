
/*
 * rl78_g23_master_boot.c
 *
 * Created on: 4 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#include "linker_config.h"

#include "ll_dfl/ll_dfl.h"
#include "ll_crc/ll_crc.h"

#include "r_smc_entry.h"

////////////////////////////
// PRIVATE CONSTANTS
//
#define BOOT_PRIORITY_START ((uint32_t)configMASTER_BOOT_DFM_START)
#define BOOT_PRIORITY_LENGTH (sizeof(uint8_t))

#define BOOT_LOADER_CRC_START ((uint32_t)BOOT_PRIORITY_START + BOOT_PRIORITY_LENGTH)
#define BOOT_LOADER_CRC_LENGTH (sizeof(uint16_t))

#define APPLICATION_CRC_START ((uint32_t)BOOT_LOADER_CRC_START + BOOT_LOADER_CRC_LENGTH)
#define APPLICATION_CRC_LENGTH (sizeof(uint16_t))



////////////////////////////
// PRIVATE TYPES
//
typedef void(*func_ptr_t)(void);



////////////////////////////
// PRIVATE TYPE CONSTANTS
//
#define INVOKE_BOOT_LOADER ((func_ptr_t)configBOOT_LOADER_CFM_START)()
#define INVOKE_APPLICATION ((func_ptr_t)configAPPLICATION_CFM_START)()



////////////////////////////
// PRIVATE ATTRIBUTES
//



////////////////////////////
// PRIVATE METHODS
//
signed int Main_masterBootInvoke(void);



////////////////////////////
// IMPLEMENTATION
//
signed int Main_masterBootInvoke(void)
{
	const uint8_t frequency = 31; // Value 31 defines 32MHz
	uint8_t bootPriority;
	uint16_t bootLoaderCrc16;
	uint16_t applicationCrc16;
	uint16_t calculatedBootLoaderCrc16;
	uint16_t calculatedApplicationCrc16;

	LL_Dfl_open(frequency); // Needed to work with the data flash
	// Reading boot priority and crrc  checksums for boot loader and application from data flash
	LL_Dfl_read(BOOT_PRIORITY_START, (uint8_t* const)&bootPriority, BOOT_PRIORITY_LENGTH);
	LL_Dfl_read(BOOT_LOADER_CRC_START, (uint8_t* const)&bootLoaderCrc16, BOOT_LOADER_CRC_LENGTH);
	LL_Dfl_read(APPLICATION_CRC_START, (uint8_t* const)&applicationCrc16, APPLICATION_CRC_LENGTH);

	LL_Crc_init(); // Calculating crc checksums of boot loader and application memory regions
	calculatedBootLoaderCrc16 = LL_Crc_calc16((const uint8_t* const)configBOOT_LOADER_CFM_START, (uint16_t)configBOOT_LOADER_CFM_LENGTH, 0);
	calculatedApplicationCrc16 = LL_Crc_calc16((const uint8_t* const)configAPPLICATION_CFM_START, (uint16_t)configAPPLICATION_CFM_LENGTH, 0);
	LL_Dfl_close();

	// Checking crc checksums
	if (bootLoaderCrc16 != calculatedBootLoaderCrc16)
	{
		goto error;
	}

	if (applicationCrc16 != calculatedApplicationCrc16)
	{
		goto error;
	}

	// Jumping to either boot loader or application depending on the boot priority flag
	if ((uint8_t)configBOOT_LOADER_PRIORITY == bootPriority)
	{
		INVOKE_BOOT_LOADER;
	}
	else if ((uint8_t)configAPPLICATION_PRIORITY == bootPriority)
	{
		INVOKE_APPLICATION;
	}
	else
	{
		goto error;
	}

error: // Should never reach!
	while (1) { NOP(); }
	return 0;
}
