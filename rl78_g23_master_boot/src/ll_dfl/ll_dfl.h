
/*
 * ll_dfl.h
 *
 * Created on: 4 Sep 2023
 *     Author: Joris Baranauskas (joris.baranauskas@emusbms.com)
 */

#ifndef __LL_DFL__LL_DFL_H__
#define __LL_DFL__LL_DFL_H__

#include <stdint.h>

void LL_Dfl_open(const uint8_t frequency);

void LL_Dfl_close(void);

void LL_Dfl_read(
	const uint32_t address20,
	uint8_t* const buffer,
	const uint16_t length);

#endif /* __LL_DFL__LL_DFL_H__ */
